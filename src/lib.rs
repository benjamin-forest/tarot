use serde::Deserialize;

#[derive(Debug, Deserialize)]
enum Bonus {
    Poignet8,
    Poignet10,
    Poignet13,
    Poignet15,
    Poignet18,
    #[serde(alias="ChelemAnnoncéFait")]
    ChelemAnnouncedDone,
    #[serde(alias="ChelemAnnoncéÉchoué")]
    #[serde(alias="ChelemAnnoncéEchoué")]
    ChelemAnnouncedFailed,
    #[serde(alias="ChelemFait")]
    ChelemDone,
}


impl Bonus {
    fn chelem(&self) -> Option<i32> {
        match self {
            Bonus::ChelemAnnouncedDone => {Some(400)},
            Bonus::ChelemAnnouncedFailed => {Some(-200)},
            Bonus::ChelemDone => {Some(200)}
            _ => {None}
        }
    }

    fn points(&self, players_count: i32) -> Option<i32> {
        let is_chelem = self.chelem();
        if is_chelem.is_some() {
            return is_chelem
        }

        if players_count == 3 {
            match self {
                Bonus::Poignet13 => {Some(20)},
                Bonus::Poignet15 => {Some(30)},
                Bonus::Poignet18 => {Some(40)},
                _ => {None}
            }
        } else if players_count == 4 {
            match self {
                Bonus::Poignet10 => {Some(20)},
                Bonus::Poignet13 => {Some(30)},
                Bonus::Poignet15 => {Some(40)},
                _ => {None}
            }
        } else if players_count == 5 {
            match self {
                Bonus::Poignet8 =>  {Some(20)},
                Bonus::Poignet10 => {Some(30)},
                Bonus::Poignet13 => {Some(40)},
                _ => {None}
            }
        } else {Some(0)}
    }
}

#[derive(Debug, Deserialize)]
enum PetitAuBout {
    #[serde(alias="Attaque")]
    Attack,
    #[serde(alias="Défense")]
    Defense,
    None,
}

impl PetitAuBout {
    fn points(&self) -> i32 {
        match self {
            PetitAuBout::Attack => {10},
            PetitAuBout::Defense => {-10},
            PetitAuBout::None => {0},
        }
    }

    fn default() -> PetitAuBout {
        PetitAuBout::None
    }
}


#[derive(Debug, Clone, Copy, Deserialize)]
pub enum Bet {
    Petite,
    Garde,
    GardeSans,
    GardeContre,
}

impl Bet {
    fn factor(&self) -> i32 {
        match self {
            Bet::Petite => {1},
            Bet::Garde => {2},
            Bet::GardeSans => {4},
            Bet::GardeContre => {6},
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Role {
    Attacker1,
    Attacker2,
    Defenser,
}


#[derive(Debug, Deserialize)]
pub struct Game {
    #[serde(alias = "attaquants")]
    attackers: Vec<String>,
    #[serde(alias = "pari")]
    bet: Bet,
    points: i32,
    #[serde(alias = "bouts")]
    oulders: i32,
    #[serde(default)]
    bonus: Vec<Bonus>,
    #[serde(default = "PetitAuBout::default")]
    petit: PetitAuBout,
}


#[derive(Debug, Deserialize)]
pub struct Event {
    #[serde(alias = "joueurs")]
    pub players: Vec<String>,
    pub lang: String,
    #[serde(alias = "parties")]
    pub games: Vec<Game>,
}

#[derive(Debug, Clone)]
struct Player {
    name: String,
    points: i32,
    role: Role,
}

#[derive(Debug, PartialEq, Clone)]
pub struct PlayerPoints {
    pub name: String,
    pub points: i32,
}


impl Game {
    fn diff_points(&self) -> Option<i32> {
        match self.oulders {
            0 => {Some(self.points - 56)}
            1 => {Some(self.points - 51)}
            2 => {Some(self.points - 41)}
            3 => {Some(self.points - 36)}
            _ => {None}
        }
    }


    fn bonus_points(&self, players_count: i32) -> i32 {
        self.bonus.iter().map(|b| b.points(players_count).unwrap()).sum()
    }

    
    fn total_points(&self, players_count: i32) -> i32 {
        let points = self.diff_points().unwrap();
        let bonus = self.bonus_points(players_count);
        let petit = self.petit.points();
        let bet_factor = self.bet.factor();

        if points >= 0 {
            (25 + points + petit) * bet_factor + bonus
        } else {
            (-25 + points + petit) * bet_factor - bonus
        }
    }


}

pub fn collect_points(games: &Event) -> Vec<Vec<PlayerPoints>> {
    let mut cumulated_points = games.players.iter().map(|joueur|
        PlayerPoints {name: joueur.clone(), points: 0}
    ).collect();

    let mut points = vec![];
    for party in &games.games {
        cumulated_points = compute_party_points(&cumulated_points, party);
        points.push(cumulated_points.clone());
    }
    
    points
}

fn compute_party_points(players: &Vec<PlayerPoints>, game: &Game) -> Vec<PlayerPoints> {
    let game_points = game.total_points(players.len() as i32);
    let player_roles = party_players_role(&game, &players).unwrap();
    compute_points(player_roles, game_points)
}

fn party_players_role(game: &Game, players: &Vec<PlayerPoints>) -> Option<Vec<Player>> {
    let attacker1 = &game.attackers[0];
    let attacker2;

    if game.attackers.len() == 1 {
        attacker2 = "";
    } else if game.attackers.len() == 2 {
        attacker2 = &game.attackers[1];
    } else {
        return None
    }

    let players_role = players.iter().map({|player| 
        if attacker1 == &player.name {
            Player {name: player.name.clone(), points: player.points, role: Role::Attacker1}
        } else if attacker2 == &player.name {
            Player {name: player.name.clone(), points: player.points, role: Role::Attacker2}
        } else {
            Player {name: player.name.clone(), points: player.points, role: Role::Defenser}
        }
    }).collect();

    Some(players_role)
}

fn compute_points(players: Vec<Player>, points: i32) -> Vec<PlayerPoints> {
    let mut have_second_attacker = false;
    let mut have_main_attacker = false;

    for player in players.iter() {
        if let Role::Attacker2 = player.role {
            have_second_attacker = true;
        } else if let Role::Attacker1 = player.role {
            have_main_attacker = true;
        }
    }

    if have_main_attacker == false {
        panic!("I need at most one attacker!");
    }

    if players.len() <= 4 && have_second_attacker {
        panic!("With four or three players, only one attacker is allowed.");
    }

    let main_attacker_ratio = match players.len() as i32 {
        5 => {if have_second_attacker == true {2} else {4}},
        ref len => {len - 1},
    };

    players.iter().map({ |player| 
        match player.role {
            Role::Defenser  => PlayerPoints {name: player.name.clone(), points: player.points - points},
            Role::Attacker2 => PlayerPoints {name: player.name.clone(), points: player.points + points},
            Role::Attacker1 => PlayerPoints {name: player.name.clone(), points: player.points + points * main_attacker_ratio},
        }
    }).collect()
}

