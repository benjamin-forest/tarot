use std::fs::File;

use prettytable::format;
use prettytable::{Table, Row, Cell};

fn main() {
  let filename = std::env::args().nth(1).expect("A yaml file");
  let game_file = File::open(filename).unwrap();
  let event: tarot::Event = serde_yaml::from_reader(game_file).unwrap();
  let all_points = tarot::collect_points(&event);

  let mut table = create_table_formatter(&event.players);
  print_games_points(&mut table, &all_points);
}

fn create_table_formatter(players: &Vec<String>) -> prettytable::Table {
  let mut table = Table::new();
  table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);

  let title = players.iter().map({ |player|
      Cell::new(&player)
  }).collect();

  table.add_row(Row::new(title));

  table
}

fn print_games_points(table: &mut Table, all_points: &Vec<Vec<tarot::PlayerPoints>>) {
  for points in all_points {
    let points_cells = points.iter().map({ |p|
         Cell::new(&format!("{:>4}", p.points))
     }).collect();
     table.add_row(Row::new(points_cells));
  }

  table.printstd();
}
