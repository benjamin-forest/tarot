const YML: &str = r#"
joueurs: [Moi, toi, lui, elle, nous]
lang: fr

parties:
 -
    attaquants: [Moi, toi]
    pari: Petite
    points: 42
    bouts: 3
    bonus: []
 -
    attaquants: [elle, lui]
    pari: Garde
    points: 54
    bouts: 2
    bonus: [PetitAuBout]
 -
    attaquants: [elle, lui]
    pari: Petite
    points: 54
    bouts: 2
    bonus: [Poignet, PetitAuBout]
 -
    attaquants: [elle, Moi]
    pari: Petite
    points: 12
    bouts: 3
    bonus: [Poignet, PetitAuBout]
"#;

#[test]
fn load() {
  let event: tarot::Event = serde_yaml::from_str(YML).unwrap();

  assert_eq!(5, event.players.len());
  assert_eq!(4, event.games.len());
}

fn find_player<'a>(players: &'a Vec<tarot::PlayerPoints>, name: &str) -> Option<&'a tarot::PlayerPoints> {
    players.iter().find(|player| &player.name == name)
}

const WITH_FOUR_PLAYERS_ONLY_ONE_ATTACKER: &str = r#"
joueurs: [Moi, toi, lui, elle]
lang: fr

parties:
 -
    attaquants: [Moi, toi]
    pari: Garde
    points: 49
    bouts: 2
    bonus: [Poignet10]
    petit: Attaque
"#;

fn with_four_players_only_one_attacker() {
    let event: tarot::Event = serde_yaml::from_str(WITH_FOUR_PLAYERS_ONLY_ONE_ATTACKER);

    let points = tarot::collect_points(&event).last().unwrap();
    assert_eq!(318, find_player(points, "Moi").unwrap().points);
    assert_eq!(-106, find_player(points, "toi").unwrap().points);
    assert_eq!(-106, find_player(points, "lui").unwrap().points);
    assert_eq!(-106, find_player(points, "elle").unwrap().points);

}

#[test]
fn end_result() {
  let event: tarot::Event = serde_yaml::from_str(YML).unwrap();
  let all_points = tarot::collect_points(&event);

  assert_eq!(4, all_points.len());
  
  let last = all_points.last().unwrap();
  assert_eq!(5, last.len());
  assert_eq!(-101, find_player(last, "Moi").unwrap().points);
  assert_eq!(-34 , find_player(last, "toi").unwrap().points);
  assert_eq!(132 , find_player(last, "lui").unwrap().points);
  assert_eq!(99  , find_player(last, "elle").unwrap().points);
  assert_eq!(-96 , find_player(last, "nous").unwrap().points);
  
}
